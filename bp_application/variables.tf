
variable "vpc_id" {
  type = string
}
variable "subnets" {
  type = list(string)
}

variable "cluster_name" {
  type = string
}

locals {
  cluster_name = "sritest-eks-${random_string.suffix.result}"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

variable "region" {
  default     = "us-west-2"
  description = "AWS region"
}
