terraform {
  backend "s3" {
    bucket = "prathima-tf-state"
    key    = "prathima-test"
    region = "us-west-2"
  }
}
